# Commond Deploy 
```
mvn clean deploy -PdeployLocal
mvn clean deploy -PdeployRemote
```

# Remote deploy with secure port 
-	Export the certificate from the server to the url console, right-click the certificate icon on your web browser
-	Create the truststore and import .cer obtained from the previous step.

```
keytool -import -file D:\sourcetree\workspace\MY_SCRIPT\jboss-rcl-dev.cer -alias jboss-rcl-dev -keystore D:\sourcetree\workspace\MY_SCRIPT\myTrustStore
```

-	Double check certificate in trust

	#List Trusted CA Certs
	keytool -list -v -keystore D:\sourcetree\workspace\MY_SCRIPT\myTrustStore


-	Using deploy command with argument 

```
mvn clean deploy -PdeployRCLDev -Djavax.net.ssl.trustStore=D:\sourcetree\workspace\MY_SCRIPT\myTrustStore -Djavax.net.ssl.trustStorePassword=changeit
```


