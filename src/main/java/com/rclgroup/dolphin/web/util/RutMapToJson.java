package com.rclgroup.dolphin.web.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RutMapToJson {
	
	public static JSONObject messageJson(String strJson) throws JSONException {
		
    	JSONObject json = new JSONObject(strJson);
    	if(json.get("resultStatus").equals("F")) {
        	json.put("resultCode", "400");
    	}else {
        	json.put("resultCode", "200");
    	}
		return json;
	}
	
	
	public static String convertResultOut2Json(Map<String, Object> resultOut) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		Map<String, Object> transform = new LinkedHashMap<String, Object>();
		
		Set<String> keyOutResultName = resultOut.keySet();
		for(String outputName : keyOutResultName) {
			Object obj = resultOut.get(outputName);
			
			
			if( obj instanceof ArrayList) {
	                
				ArrayList resultArrList = (ArrayList) obj;
				
				List list = new LinkedList();
				boolean isFirst = true;
				for(Object row : resultArrList) {
					Map<String, Object> temp = new LinkedHashMap<String, Object>();
					LinkedCaseInsensitiveMap<Object> r = (LinkedCaseInsensitiveMap<Object>) row;
					for(Entry<String, Object> c : r.entrySet()) {
						//System.out.println("KEY = "  + c.getKey() + ", Value = " + c.getValue());
						//String fieldNmae = WordUtils.capitalizeFully(c.getKey(), '_');
						String fieldName = StringUtils.capitalize(c.getKey());
						temp.put(fieldName, c.getValue());
						
//						if( isFirst ) {
//							Object o = c.getValue();
//							String dataTypeInfo = "fieldName : " + fieldName + ", Type : " + (o == null ? "null" : o.getClass()) ;
//							logger.debug(dataTypeInfo);
//						}
					}
					//System.out.println(row.toString());
					isFirst = false;
					list.add(temp);
				}
				
				transform.put(outputName, list);
			}else if(obj instanceof BigDecimal){
				BigDecimal number = (BigDecimal) obj;
				transform.put(outputName, number);
				
			}else if(obj instanceof String){
				transform.put(outputName, obj);
				
			}else if( obj == null ) {
				//Skip null
			}else {
				throw new Exception("Not found object type. type is " + obj.getClass());
			}
		}
		
		
		
		String json = new ObjectMapper().writeValueAsString(transform);
		System.out.println(json);
		return json;
	}
}
