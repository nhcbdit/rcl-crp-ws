package com.rclgroup.dolphin.web.ws;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

@Configuration
public class JasperReportConfig {
	private static Logger logger = LoggerFactory.getLogger(JasperReportConfig.class);

	@Autowired
	private ResourceLoader resourceLoader;

	public JasperReport getJasperReport(String jasperTemplateName) throws Exception {
		logger.info("Input Jasper template {}", jasperTemplateName);


		InputStream inputstream = getFile(jasperTemplateName);
		
		JasperReport jasperReport = JasperCompileManager.compileReport(inputstream);
		if( jasperReport != null ) {
			return jasperReport;
		}
		
		throw new FileNotFoundException("File not found : " + jasperTemplateName);
	}
	
	
	public InputStream getFile(String templateFilename) throws Exception {
		String fullpathFilename = templateFilename;
		
		try {
			logger.info("Get Report Filename : {} ", templateFilename);
		
			ClassLoader classLoader = getClass().getClassLoader();
			URL resource = classLoader.getResource(fullpathFilename);
			if (resource == null) {
				throw new IllegalArgumentException("file is not found!");
			}

			logger.info("Load template at {}", resource.getPath());

			InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(fullpathFilename);
			
			return resourceAsStream;
		} catch (Exception ex) {
			logger.info("Exception .. , fail load file at {} ", fullpathFilename);
			
			InputStream resourceAsStream = null;
			try {
				fullpathFilename = templateFilename;
				resourceAsStream = getClass().getClassLoader().getResourceAsStream(fullpathFilename);
			
				return resourceAsStream;
			} finally {
				if( resourceAsStream != null ) {
					resourceAsStream.close();
				}
			}
		}
	}

}
