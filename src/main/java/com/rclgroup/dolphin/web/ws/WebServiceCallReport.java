package com.rclgroup.dolphin.web.ws;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rclgroup.dolphin.web.auth.BrowserData;
import com.rclgroup.dolphin.web.common.RrcApplicationContextWS;
import com.rclgroup.dolphin.web.config.ScheduleGenReport;
import com.rclgroup.dolphin.web.config.ScheduleGenReport.HistoryNoticeLog;
import com.rclgroup.dolphin.web.config.ScheduleGenReport.ScheduleLog;
import com.rclgroup.dolphin.web.dao.crp.CrpCommonDao;
import com.rclgroup.dolphin.web.dao.crp.CrpVoyageDao;
import com.rclgroup.dolphin.web.dao.crp.CrpVoyageDao.ProcedureResult;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.exception.ResponseMsgJson;
import com.rclgroup.dolphin.web.model.crp.DataPredictionReportMod;
import com.rclgroup.dolphin.web.model.crp.FileInfo;
import com.rclgroup.dolphin.web.model.crp.reqest.DataPredictionItem;
import com.rclgroup.dolphin.web.model.crp.reqest.ReqCurrentPort;
import com.rclgroup.dolphin.web.model.crp.reqest.ReqSearchVoyageExportMod;
import com.rclgroup.dolphin.web.model.crp.reqest.ReqSearchVoyageMod;
import com.rclgroup.dolphin.web.model.crp.reqest.ReqVoyageInfo;
import com.rclgroup.dolphin.web.model.rcm.RcmUserMod;
import com.rclgroup.dolphin.web.service.crp.GenerateExcelReportService;
import com.rclgroup.dolphin.web.service.crp.GenerateJasperReportService;
import com.rclgroup.dolphin.web.util.RutMapToJson;
import com.rclgroup.dolphin.web.ws.TokenAuthUtils.AuthResult;


@Path("/crp")
public class WebServiceCallReport extends WebServiceBase {
	private Logger logger = LoggerFactory.getLogger(WebServiceCallReport.class);
	
	public static final String USER_DISABLED = "USER_DISABLED";

	@Context
	private ServletConfig servletConfig;
	@Context
	private ServletContext servletContext;
	@Context
	private HttpServletRequest httpServletRequest;
	@Context
	private HttpServletResponse httpServletResponse;
	@Context 
	private HttpHeaders headers;
	
//	protected RcmUserMod getRcmUserMod() {
//		AuthResult auth = (AuthResult) httpServletRequest.getSession().getAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT);
//		
//		if( auth == null ) {
//			logger.error("session has expired.");
//			
//			String jsonErrorMsg = ResponseMsgJson.messageString("401", "session has expired.", "", "F");
//			Response errResponse = Response.status(Status.UNAUTHORIZED).entity(jsonErrorMsg).build();
//
//        	throw new WebApplicationException(errResponse);
//		}
//		
//		LoginUser loginUser = auth.getLoginUser();
//		RcmUserMod rcmUserMod = loginUser.getUserMod();
//
//
//		return rcmUserMod;
//	}
	
	protected RcmUserMod getRcmUserMod() {
		RcmUserMod rcmUserMod = super.getRcmUserMod(httpServletRequest);
//		
//        RcmUserMod rcmUserMod = new RcmUserMod();
//        rcmUserMod.setPrsnLogId("DEV_TEAM");

		return rcmUserMod;
	}
	

	protected void authentication() throws Exception {
		if( logger.isDebugEnabled() ) {
			logger.debug("-- Servlet Request --");
			logger.debug("Request URI: " + httpServletRequest.getRequestURI());
			Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
			ArrayList<String> list = Collections.list(headerNames);
			logger.debug("Request headers: " + list.size());
			list.forEach(h -> System.out.println(h + " = " + httpServletRequest.getHeader(h)));
			
			Enumeration<String> sessionNames = httpServletRequest.getSession().getAttributeNames();
			ArrayList<String> listSessionName = Collections.list(sessionNames);
			logger.debug("Request session: " + listSessionName.size());
			listSessionName.forEach(h -> System.out.println(h + " = " + httpServletRequest.getSession().getAttribute(h)) );
		}
		
		HttpSession session = httpServletRequest.getSession(true);
		Object rclAuthResultObj = session.getAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT);
		
		List<String> authHeaders = headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
		if( authHeaders != null && !authHeaders.isEmpty()) {
			String browserDataEncode = authHeaders.get(0);
			
			byte[] decodedBytes = Base64.getDecoder().decode(browserDataEncode);
			String decodedString = new String(decodedBytes);
			
			JSONObject json = new JSONObject(decodedString);
			 
			BrowserData browserData = null;
	        try {
				browserData = new ObjectMapper().readValue(json.toString(), BrowserData.class);
			    
				if( rclAuthResultObj != null ) {
					AuthResult rclAuthResult = (AuthResult) rclAuthResultObj;
					String sessionUserToken = rclAuthResult.getLoginUser().getUserToken();
					String headerUserToken = browserData.getUserToken();
					
					if( sessionUserToken != null && sessionUserToken.equals(headerUserToken)) {
						logger.info("Using session token " + sessionUserToken);
						return;
					}
				}
				
		        AuthResult result = TokenAuthUtils.checkToken(browserData, session);
		        
		        if(result.getErrorMessage() != null) {
		        	logger.error("TokenAuthUtils.checkToken has error message : " + result.getErrorMessage());
		        	
		        	String jsonErrorMsg = ResponseMsgJson.messageString("401", "Session expire", "", "F");

	    			Response errResponse = Response.status(Status.UNAUTHORIZED).entity(jsonErrorMsg).build();

	            	throw new WebApplicationException(errResponse);
	            }
	            
	            synchronized(session) {
	            	Object counterObj = session.getAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER);
	                if(counterObj == null) {
	                	session.setAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER, 1);
	                }else {
	                	session.setAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER, (int)counterObj + 1);
	                }
	                session.setAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT, result);
	            }
	            
	            
	            
			} catch (JsonProcessingException | JSONException ex) {
				logger.error(ex.getMessage(), ex);
				throw ex;
			} 
	        
		}else {
			logger.error("Require Authorization header");
			
			String jsonErrorMsg = ResponseMsgJson.messageString("401", "Access denied", "", "F");
			Response errResponse = Response.status(Status.UNAUTHORIZED).entity(jsonErrorMsg).build();

        	throw new WebApplicationException(errResponse);
		}
		
	}
	
	
	private Map<String, Object> getPagingInfo(int itemPerPage, int pageNo, int totalRecord){
		int startOffset = (pageNo - 1) * itemPerPage;
		
		int totalPage = (int) Math.ceil((float)totalRecord/itemPerPage);
		//Last
		int lastOffset = (totalPage - 1) * itemPerPage;
		
		//Previous
		int previousOffset = ((pageNo - 1) - 1) * itemPerPage;

		//Next
		int nextOffset = ((pageNo + 1) - 1) * itemPerPage;
		
		Map<String, Object> pagingInfo = new HashMap<String, Object>();
		pagingInfo.put("pageNo", pageNo);
		pagingInfo.put("pageSize", itemPerPage);
		pagingInfo.put("totalPage", totalPage);
		pagingInfo.put("totalRecord",totalRecord);
		pagingInfo.put("startOffset", startOffset);
		pagingInfo.put("lastOffset", lastOffset);
		pagingInfo.put("previousOffset", previousOffset);
		pagingInfo.put("nextOffset", nextOffset);
		
		return pagingInfo;
	}
	 
	private String getRedirectDownloadTargetEndpoint() {
		HttpServletRequest httpRequest = httpServletRequest;
		
    	String uri = httpRequest.getScheme() + "://" +
    			     httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath();
            		 
    	String targetEndpoint = uri + "/rclws/crp/download";
    	
    	return targetEndpoint;
	}
	
	@GET
	@Path("/voyage-status")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getVoyageStatus(@Context HttpServletRequest request, @RequestParam String params) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			
			CrpCommonDao crpCommonDao = (CrpCommonDao) RrcApplicationContextWS.getBean(CrpCommonDao.class);
			Map<String, Object> resultOut = crpCommonDao.voyageStatusList(parameters);
			
			jsonStr = RutMapToJson.convertResultOut2Json(resultOut);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
			
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	@POST
	@Path("/voyage-list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response getVoyageList(ReqSearchVoyageMod m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			
			parameters.put("P_I_PAGE_ROWS", m.getPageRows() );
			parameters.put("P_I_PAGE_NO", m.getPageNo() );
			parameters.put("P_I_SORT", m.getSort() );
			parameters.put("P_I_ASC_DESC", m.getAscDesc() );
			parameters.put("P_I_DURATION", m.getDuration() );
			parameters.put("P_I_PORT", m.getPort() );
			parameters.put("P_I_SERVICE", m.getService() );
			parameters.put("P_I_VESSEL", m.getVessel() );
			parameters.put("P_I_VOYAGE", m.getVoyage() );
			parameters.put("P_I_REGION", m.getRegion() );
			parameters.put("P_I_COUNTRY", m.getCountry() );
			parameters.put("P_I_STATUS", m.getStatus() );
			parameters.put("P_I_SERVICE_PIC", m.getServicePic() );
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			
			Map<String, Object>  resultOut = crpVoyageDao.findVoyageList(parameters);
			BigDecimal rowTotal = (BigDecimal) resultOut.get("P_O_ROW_TOTAL");

			Map<String, Object> pagingInfo = getPagingInfo(m.getPageRows(), m.getPageNo(), rowTotal.intValue());
			resultOut.put("pagingInfo", pagingInfo);
			
			return Response.status(httpStatus).entity(resultOut).build();
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
			
			return Response.status(httpStatus).entity(jsonStr).build();
		}

    }
	
	@POST
	@Path("/current-port")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentPort(ReqCurrentPort m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();
	
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());

			parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
			parameters.put("P_I_NEXT_PREV", m.getNextPrev() );
			
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.findVoyageGetCurrentPort(parameters);
			
			jsonStr = RutMapToJson.convertResultOut2Json(resultOut);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
			
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	
	@POST
	@Path("/data-prediction")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response getDataPrediction(ReqVoyageInfo m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());

			parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
			
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.findVoyageGetPrediction(parameters);
			
			jsonStr = RutMapToJson.convertResultOut2Json(resultOut);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	@POST
	@Path("/call-report-history")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response getCallReportHistory(ReqVoyageInfo m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());

			parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
			
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.findVoyageGetCallReportHistory(parameters);
			
			jsonStr = RutMapToJson.convertResultOut2Json(resultOut);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	@POST
	@Path("/call-report-history-prediction")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response getCallReportHistoryPrediction(ReqVoyageInfo m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();


			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			
			parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.findVoyageGetCallReportHistoryPrediction(parameters);
			
			jsonStr = RutMapToJson.convertResultOut2Json(resultOut);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	private void saveCallReport(ReqVoyageInfo m) throws Exception {
	
		RcmUserMod rcmUserMod = getRcmUserMod();

		CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
		
		List<DataPredictionItem> dataPredictionList = m.getDataPredictionList();
		
		//Using thread 
		//1) Prepare Task
		ExecutorService executor = Executors.newFixedThreadPool(5);
		
        List<Callable<ProcedureResult>> callableTasks = new ArrayList<>();
        for(DataPredictionItem d : dataPredictionList) {
        	
        	Callable<ProcedureResult> callableTaskItem = () -> {

        		Map<String, Object> param = new HashMap<String, Object>();
        		param.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
        		param.put("P_I_IND_ID", d.getIndId() );
        		param.put("P_I_PREDICTION_DATA", d.getPredictionData() );
    			
        		Map<String, Object> paramOut = crpVoyageDao.voyageIndUpd(param);
        		
        		ProcedureResult procedureResult = new ProcedureResult();
        		procedureResult.setValid((String) paramOut.get("P_O_VALID"));
        		procedureResult.setErrorMsg((String) paramOut.get("P_O_ERROR_MSG"));
        		
        	    return procedureResult;
        	};
        	
        	callableTasks.add(callableTaskItem);
        }
        
        //2) Execute
		try {
			List<Future<ProcedureResult>> futures = executor.invokeAll(callableTasks);
			logger.debug("Before Future Result");

			// block until future returned a result,
			// timeout if the future takes more than 5 seconds to return the result
			for (int i = 0; i < futures.size(); i++ ) {
				Future<ProcedureResult> f = futures.get(i);
				ProcedureResult procResult = f.get();
				
				logger.debug("Future Result[" + i + "] Valid:{}, ErrorMsg:{}", procResult.getValid(), procResult.getErrorMsg());
				
				if( !"Y".equals(procResult.getValid()) ) {
					String errorMsg = procResult.getErrorMsg();
					throw new CustomDataAccessException(errorMsg);
				}
			}
			// System.err.println("Get future result : " + allDataResult);
			logger.debug("After Future Result");
		} catch (InterruptedException ex) {// thread was interrupted
			logger.error(ex.getMessage(), ex);
		} catch (ExecutionException ex) {// thread threw an exception
			logger.error(ex.getMessage(), ex);
		} finally {
			// shut down the executor manually
			executor.shutdown();
			try {
				if (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
					executor.shutdownNow();
				}
			} catch (InterruptedException e) {
				executor.shutdownNow();
			}
		}
       
		
		Map<String, Object> resultOut = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
		parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
		parameters.put("P_I_SPECIAL_INSTRUCTION", m.getSpecialInstruction() );
		
		resultOut = crpVoyageDao.voyageUpd(parameters);
		
		if( !"Y".equals(resultOut.get("P_O_VALID")) ) {
			String errorMsg = (String) resultOut.get("P_O_ERROR_MSG");
			throw new CustomDataAccessException(errorMsg);
		}
		
	
	}
	
	
	
	@POST
	@Path("/save-call-report")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response saveCallReport(ReqVoyageInfo m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			saveCallReport(m);
			
			//Success 
			Map<String, Object> responseMsg = new HashMap<String, Object>();
			responseMsg.put("resultStatus", "S");
			responseMsg.put("resultCode", "000");
			responseMsg.put("resultMsg", "Save successful");
			
			jsonStr = RutMapToJson.convertResultOut2Json(responseMsg);
		} catch (WebApplicationException ex ) {
			throw ex;
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	@POST
	@Path("/send-call-report")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response sendCallReport(ReqVoyageInfo m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();

			saveCallReport(m);
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			parameters.put("P_I_VOYAGE_ID", m.getVoyageId() );
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.voyageSubmit(parameters);
			
			if( !"Y".equals(resultOut.get("P_O_VALID")) ) {
				String errorMsg = (String) resultOut.get("P_O_ERROR_MSG");
				throw new CustomDataAccessException(errorMsg);
			}
			
			Map<String, Object> responseMsg = new HashMap<String, Object>();
			responseMsg.put("resultStatus", "S");
			responseMsg.put("resultCode", "000");
			responseMsg.put("resultMsg", "Call report sent");
			
			jsonStr = RutMapToJson.convertResultOut2Json(responseMsg);
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
    }
	
	/*
	@GET
	@Path("/data-prediction/rpt/{id}")
	@Produces("application/pdf")
	public Response generateReport(
			@PathParam("id") String voyageIdStr,
			@DefaultValue("location") @QueryParam("type") String rptType) throws Exception {

		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();

		logger.info("<== Get WebServiceCallReport [MethodName:{}] ==>", methodName);

		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();
			
			Integer voyageId = Integer.parseInt(voyageIdStr);
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			parameters.put("P_I_VOYAGE_ID", voyageId );
			
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.callReport(parameters);
			List<DataPredictionReportMod> dataPrediction = (List<DataPredictionReportMod>) resultOut.get("P_O_DATA");
			
			GenerateJasperReportService rptService = 
					(GenerateJasperReportService) RrcApplicationContextWS.getBean(GenerateJasperReportService.class);
			
			ByteArrayOutputStream baos = rptService.createCallReport(rptType, dataPrediction);
			
			String strReportName = "Call_Report";
			if( "location".equals(rptType) ) {
				strReportName += "_Location";
			}else if( "hq".equals(rptType) ) {
				strReportName += "_HQ";
			}
			
			strReportName += ".pdf";;
			final String reportFilename = "attachment;filename=\"" + strReportName + "\"";
			final String contentType = "application/pdf";
			StreamingOutput streamingOutput = getStreamingOutput(baos);

			logger.info("<== Get WebServiceCallReport [MethodName:{}] success. ==>", methodName);
			
	        Response.ResponseBuilder responseBuilder = Response.ok(streamingOutput, contentType);
	        responseBuilder.header("Content-Disposition", reportFilename);
	        responseBuilder.header("fileName", strReportName);
	        return responseBuilder.build();
	        
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			String jsonStr = json.toString();
			
			logger.error(ex.getMessage(), ex);
			return Response.status(httpStatus).entity(jsonStr).build();
		}
		
	}
	*/
	
	@GET
	@Path("/data-prediction/rpt/{id}")
	@Produces("application/pdf")
	public Response generateReport(
			@PathParam("id") String voyageIdStr,
			@DefaultValue("location") @QueryParam("type") String rptType) throws Exception {

		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();

		logger.info("<== Get WebServiceCallReport [MethodName:{}] ==>", methodName);

		Response.Status httpStatus = Status.OK;
		
		try {
//			authentication();
			
			/*
			String userId = null;
			String token = httpServletRequest.getParameter("token");
			if( token != null) {
				String browserDataEncode = token;
				
				byte[] decodedBytes = Base64.getDecoder().decode(browserDataEncode);
				String decodedString = new String(decodedBytes);
				
				JSONObject json = new JSONObject(decodedString);
				 
				BrowserData browserData = null;
		        try {
					browserData = new ObjectMapper().readValue(json.toString(), BrowserData.class);
					userId = browserData.getUserId();
		        }catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
					
					logger.error("Require invalid value");
					
					String jsonErrorMsg = ResponseMsgJson.messageString("401", "Access denied", "", "F");
					Response errResponse = Response.status(Status.UNAUTHORIZED).entity(jsonErrorMsg).build();

		        	throw new WebApplicationException(errResponse);
				}
			}else {
				logger.error("Require Token.");
				
				String jsonErrorMsg = ResponseMsgJson.messageString("401", "Access denied", "", "F");
				Response errResponse = Response.status(Status.UNAUTHORIZED).entity(jsonErrorMsg).build();

	        	throw new WebApplicationException(errResponse);
			}
			*/
		        		
			Integer voyageId = Integer.parseInt(voyageIdStr);
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			//parameters.put("P_I_USER_ID", userId);
			parameters.put("P_I_USER_ID", "DEV_TEAM");
			parameters.put("P_I_VOYAGE_ID", voyageId );
			
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.callReport(parameters);
			List<DataPredictionReportMod> dataPrediction = (List<DataPredictionReportMod>) resultOut.get("P_O_DATA");
			
			GenerateJasperReportService rptService = 
					(GenerateJasperReportService) RrcApplicationContextWS.getBean(GenerateJasperReportService.class);
			
			ByteArrayOutputStream baos = rptService.createCallReport(rptType, dataPrediction);
			
			String strReportName = "Call_Report";
			if( "location".equals(rptType) ) {
				strReportName += "_Location";
			}else if( "hq".equals(rptType) ) {
				strReportName += "_HQ";
			}
			
			strReportName += ".pdf";;
			final String reportFilename = "attachment;filename=\"" + strReportName + "\"";
			final String contentType = "application/pdf";
			StreamingOutput streamingOutput = getStreamingOutput(baos);

			logger.info("<== Get WebServiceCallReport [MethodName:{}] success. ==>", methodName);
			
	        Response.ResponseBuilder responseBuilder = Response.ok(streamingOutput, contentType);
	        responseBuilder.header("Content-Disposition", reportFilename);
	        responseBuilder.header("fileName", strReportName);
	        return responseBuilder.build();
	        
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			String jsonStr = json.toString();
			
			logger.error(ex.getMessage(), ex);
			return Response.status(httpStatus).entity(jsonStr).build();
		}
		
	}

	private StreamingOutput getStreamingOutput(final ByteArrayOutputStream byteArrayOutputStream) {
	    return new StreamingOutput() {
	        public void write(OutputStream output) throws IOException, WebApplicationException {
	            byteArrayOutputStream.writeTo(output);
	        }
	    };
	}
	
	
	@POST
	@Path("/schedule-report")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response scheduleMonitoringReportExcel(ReqSearchVoyageExportMod m, @Context HttpHeaders headers) throws Exception {
		String jsonStr = "";
		String methodName = new Object() {}.getClass().getEnclosingMethod().getName();

		logger.info("<== Get WebServiceCallReport [MethodName:{}] ==>", methodName);
		
		Response.Status httpStatus = Status.OK;
		
		try {
			authentication();
			RcmUserMod rcmUserMod = getRcmUserMod();
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_I_USER_ID", rcmUserMod.getPrsnLogId());
			parameters.put("P_I_PORT", m.getPort() );
			parameters.put("P_I_SERVICE", m.getService() );
			parameters.put("P_I_VESSEL", m.getVessel() );
			parameters.put("P_I_VOYAGE", m.getVoyage() );
			parameters.put("P_I_REGION", m.getRegion() );
			parameters.put("P_I_COUNTRY", m.getCountry() );
			parameters.put("P_I_STATUS", m.getStatus() );
			parameters.put("P_I_SERVICE_PIC", m.getServicePic() );
			parameters.put("P_I_ETA_FROM", m.getEtaFrom());
			parameters.put("P_I_ETA_TO", m.getEtaTo());
		
			
			CrpVoyageDao crpVoyageDao = (CrpVoyageDao) RrcApplicationContextWS.getBean(CrpVoyageDao.class);
			Map<String, Object> resultOut = crpVoyageDao.monitorReport(parameters);

			
			GenerateExcelReportService excelRptService = RrcApplicationContextWS.getBean(GenerateExcelReportService.class);

			final String strReportName = "schedule-report.xlsx";
	        ByteArrayOutputStream byteOut = excelRptService.exportScheduleMonitoringReport(resultOut);
	        byte[] excelDocument = byteOut.toByteArray();
	        
	        
	        String uuid = UUID.randomUUID().toString();
	        FileInfo docInfo = new FileInfo(uuid, strReportName, "excel", excelDocument);
	        httpServletRequest.getSession().setAttribute(uuid, docInfo);
			logger.info("[SessionID:{}] Set Attribute name : {}" ,httpServletRequest.getSession().getId(), uuid);
			
			
			//Set Response message  
			CacheControl cc = new CacheControl();
		    cc.setMaxAge(0);
		    cc.setPrivate(true);
		    cc.setMustRevalidate(true);

		    //Get redirect URL for download file
	        String targetEndpoint = getRedirectDownloadTargetEndpoint();
	        
	        
			
			Map<String, Object> responseMsg = new HashMap<String, Object>();
			responseMsg.put("resultStatus", "S");
			responseMsg.put("resultCode", "000");
			responseMsg.put("resultMsg", "Call Report export");
			
			jsonStr = RutMapToJson.convertResultOut2Json(responseMsg);
			
			Response.ResponseBuilder rb = Response.status(200).entity(jsonStr);
		    Response response = rb
		                        .header("download_url", targetEndpoint + "/" + uuid)
		                        .build();
		    return response;
		      
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
		}

		return Response.status(httpStatus).entity(jsonStr).build();
		
	}
	
	@GET
    @Path("/download/{uuid}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadFileWithGet(@PathParam("uuid") String uuid, @Context HttpServletRequest request) {
		Response.Status httpStatus = Status.OK;
		
		try {
			
			FileInfo docInfo = (FileInfo) request.getSession().getAttribute(uuid);
			byte[] excelDocument = docInfo.getByteDocument();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream(excelDocument.length);
			baos.write(excelDocument, 0, excelDocument.length);
			StreamingOutput streamingOutput = getStreamingOutput(baos);

	        Response.ResponseBuilder responseBuilder = Response.ok(streamingOutput, "Application/excel");
	        responseBuilder.header("Content-Disposition", "attachment;filename=" + docInfo.getFilename());
	        return responseBuilder.build();
	        
		}catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			String jsonStr = json.toString();
			
			return Response.status(httpStatus).entity(jsonStr).build();
		}

    }
	
	
	@GET
	@Path("/notice-history")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getNoticeHistory(@Context HttpServletRequest request) throws Exception {
		String jsonStr = "";
		
		Response.Status httpStatus = Status.OK;
		
		try {

			ScheduleGenReport schedule = (ScheduleGenReport) RrcApplicationContextWS.getBean(ScheduleGenReport.class);
			List<HistoryNoticeLog> noticeHistoryList = schedule.getHistoryNotice();
			List<ScheduleLog> scheduleLogList = schedule.getHistorySchedule();
			
			Map<String, Object> resultOut = new HashMap<String, Object>();
			resultOut.put("scheduleLog", scheduleLogList);
			resultOut.put("noticeHistory", noticeHistoryList);
			
			return Response.status(httpStatus).entity(resultOut).build();
			
		} catch (Exception ex) {
			httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
			
			logger.error(ex.getMessage(), ex);
			JSONObject json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Internal System", "F");
			jsonStr = json.toString();
			
			return Response.status(httpStatus).entity(jsonStr).build();
		}
    }

}
