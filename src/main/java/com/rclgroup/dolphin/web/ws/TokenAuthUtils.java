package com.rclgroup.dolphin.web.ws;

import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.rclgroup.dolphin.web.auth.BrowserData;
import com.rclgroup.dolphin.web.auth.IAuth;
import com.rclgroup.dolphin.web.auth.LoginUser;
import com.rclgroup.dolphin.web.auth.Result;
import com.rclgroup.dolphin.web.auth.TempAuth;
import com.rclgroup.dolphin.web.auth.dao.IAuthDao;
import com.rclgroup.dolphin.web.common.RrcApplicationContextWS;
import com.rclgroup.dolphin.web.dao.rcm.RcmUserDao;
import com.rclgroup.dolphin.web.model.qtn.CustomerBean;
import com.rclgroup.dolphin.web.model.rcm.RcmUserMod;

public class TokenAuthUtils {

	private static IAuth authClient;
	
	public static class AuthResult {
		private LoginUser loginUser;
		private CustomerBean customerBean;
		private String errorMessage;
		
		public AuthResult(LoginUser loginUser, CustomerBean customerBean, String errorMessage) {
			this.loginUser = loginUser;
			this.customerBean = customerBean;
			this.errorMessage = errorMessage;
		}

		/**
		 * @return the loginUser
		 */
		public LoginUser getLoginUser() {
			return loginUser;
		}

		/**
		 * @return the customerBean
		 */
		public CustomerBean getCustomerBean() {
			return customerBean;
		}

		/**
		 * @return the errorMessage
		 */
		public String getErrorMessage() {
			return errorMessage;
		}
	}
	
	private static void tryInitAuth(HttpSession session) {
		if(authClient == null) {
			int timeout = 30;
			if(session != null && session.getMaxInactiveInterval() > 0) {
				timeout = session.getMaxInactiveInterval()/60;
			}
			authClient = new TempAuth(
					(IAuthDao) RrcApplicationContextWS.getBean("authDao"), 
					(RcmUserDao) RrcApplicationContextWS.getBean("rcmUserDao"),
					timeout, 30*60);
		}
	}
	
	/**
	 * Check token validity, must be checked before accessing session
	 * @throws SQLException 
	 */
	public static AuthResult checkToken(BrowserData browserData, HttpSession session)  {
		try {
			tryInitAuth(session);
			Result authResult = authClient.verifyToken(browserData, session);
			if(authResult.isSuccess()) {
				LoginUser user = authResult.getUserData();
				RcmUserMod userMod = user.getUserMod();
				CustomerBean custBean = new CustomerBean();
				custBean.setUserId(userMod.getPrsnLogId());
				custBean.setLine(userMod.getFscLvl1());
				custBean.setTrade(userMod.getFscLvl2());
				custBean.setAgent(userMod.getFscLvl3());
				custBean.setFSC_CODE(userMod.getFscCode());
				custBean.setMainCurr(userMod.getSmcurr());
				custBean.setCountry(userMod.getCountry());
				custBean.setFSC_NAME(userMod.getFscName());
				custBean.setDESCR(userMod.getDescr());
				custBean.setFSC_DATE_FORMAT(userMod.getFscDateFormat());
				
				return new AuthResult(user, custBean, null);
				
			}else {
				//return new AuthResult(null, null, "Session expired");
				JSONObject json = new JSONObject();
				json.put("Success", false);
				json.put("Content", "Session expired");
				json.put("Auth", true);
				json.put("resultCode", "401");
				json.put("Type", "expire");
				return new AuthResult(null, null, json.toString());
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new AuthResult(null, null, "CheckToken Exception : "+e.getMessage());
		}
	}
}
