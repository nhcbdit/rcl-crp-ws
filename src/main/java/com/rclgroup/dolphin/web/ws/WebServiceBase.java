package com.rclgroup.dolphin.web.ws;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.rclgroup.dolphin.web.auth.LoginUser;
import com.rclgroup.dolphin.web.auth.UserAuthority;
import com.rclgroup.dolphin.web.model.rcm.RcmUserMod;
import com.rclgroup.dolphin.web.ws.TokenAuthUtils.AuthResult;

public abstract class WebServiceBase {

	public static final String SESSION_RCL_AUTHRESULT = "rclAuthresult";
	public static final String SESSION_RCL_USAGECOUNTER = "rclUsageCnt";
	
	public AuthResult getAuthResult(HttpServletRequest request) {
		return (AuthResult) request.getSession().getAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT);
	}
	
	public RcmUserMod getRcmUserMod(HttpServletRequest request) {
		AuthResult auth = this.getAuthResult(request);
		
		LoginUser loginUser = auth.getLoginUser();
		RcmUserMod rcmUserMod = loginUser.getUserMod();
		
		return rcmUserMod;
	}
	
	
	public UserAuthority hasAuthority(HttpServletRequest request, String menuId) {
		AuthResult result = this.getAuthResult(request);
	
		UserAuthority authority = result.getLoginUser().getAuthority(menuId);
		
		return authority;
	}
	
	public List<UserAuthority> hasAuthority(HttpServletRequest request, String... menuIdArg ) {
		List<UserAuthority> temp = new LinkedList<UserAuthority>();
		if(menuIdArg.length > 0) {
			
			for(String m : menuIdArg) {
				UserAuthority u = hasAuthority(request, m);
				temp.add(u);
			}
		}
		
		return temp;
	}
}
