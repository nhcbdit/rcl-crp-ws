package com.rclgroup.dolphin.web.service.crp;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.StringUtils;

@Service
public class GenerateExcelReportService {
	private static Logger logger = LoggerFactory.getLogger(GenerateExcelReportService.class);
			
	private String pahtReportTemplate = "excel_template/";
	
	
	public ByteArrayOutputStream exportScheduleMonitoringReport(Map<String, Object> resultOut) throws Exception {
		
		Workbook workbook = null;
		try {
			Object pOutData = resultOut.get("P_O_DATA");
			
			List<Object[]> dataList = convertResultOut2ArrayObject(pOutData);
			
			String reportTemplate = pahtReportTemplate + "schedule-report.xlsx";
			WriteExcelReportUtil wExcelReportHelper = new WriteExcelReportUtil(reportTemplate) {
				@Override
				public void overrideSheet(Sheet sheet) {
					//No override because we using template file.
				}
			};
			
		    workbook = wExcelReportHelper.getWorkbook();
	    	List<Object[]> contentData = new LinkedList<Object[]>();
    	

			Object[] obj = null;
			for (int i = 0; i < dataList.size(); i++) {
    			
//    			obj = new Object[] {
//    					i+1,//No
//    					null
//    			};
				
				obj = dataList.get(i);
    			
    			contentData.add(obj);
    		}
			

			//Generate excel sheet
    		Sheet sheet = wExcelReportHelper.getExistingSheet("Data");
    		//wExcelReportHelper.setCellValue(sheet, 0, 9, timestampDisplay);
    		wExcelReportHelper.writeReport(sheet, contentData, 0, 1);
    		wExcelReportHelper.setAutoSizeColumn(sheet);
    		
    		return wExcelReportHelper.getByteArrayOutputStream();
    	}catch(Exception ex) {
    		logger.error(ex.getMessage(), ex);
    		throw ex;
    	}finally {
    		//Close workbook
    		if( workbook != null ) {
    			workbook.close();
    		}
		}
		
	}
	
	private List<Object[]> convertResultOut2ArrayObject(Object pOutData) throws Exception {
		List<Object[]> result = new ArrayList<Object[]>();
		
		if( pOutData instanceof ArrayList) {
                
			ArrayList resultArrList = (ArrayList) pOutData;

			boolean isFirst = true;
			for(Object row : resultArrList) {
				Map<String, Object> temp = new LinkedHashMap<String, Object>();
				LinkedCaseInsensitiveMap<Object> r = (LinkedCaseInsensitiveMap<Object>) row;
				for(Entry<String, Object> c : r.entrySet()) {
					//System.out.println("KEY = "  + c.getKey() + ", Value = " + c.getValue());
					//String fieldNmae = WordUtils.capitalizeFully(c.getKey(), '_');
					String fieldName = StringUtils.capitalize(c.getKey());
					temp.put(fieldName, c.getValue());
					
//						if( isFirst ) {
//							Object o = c.getValue();
//							String dataTypeInfo = "fieldName : " + fieldName + ", Type : " + (o == null ? "null" : o.getClass()) ;
//							logger.debug(dataTypeInfo);
//						}
				}
				
				if( temp.size() > 0 ) {
					Object[] record = temp.values().toArray(new Object[0]);
					result.add(record);
				}

			}
		
		}else {
			throw new Exception("Not found object type. type is " + pOutData.getClass());
		}
		
		return result;
	}
}
