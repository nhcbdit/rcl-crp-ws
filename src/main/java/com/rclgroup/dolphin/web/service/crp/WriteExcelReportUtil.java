package com.rclgroup.dolphin.web.service.crp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;


public class WriteExcelReportUtil {
	private Logger logger = org.slf4j.LoggerFactory.getLogger(WriteExcelReportUtil.class);

	protected Workbook workbook;

	public WriteExcelReportUtil() throws IOException {
		// Create a Workbook
		this.workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
	}
	
	public WriteExcelReportUtil(String templateFile) throws Exception {
//		ClassLoader classLoader = getClass().getClassLoader();
//		URL resource = classLoader.getResource(templateFile);
//	
//		File file = new File(resource.getPath());
//		FileInputStream inputStream = new FileInputStream(file);

		InputStream templateInputStream = getFile(templateFile);
		this.workbook = WorkbookFactory.create(templateInputStream);
	}
	
	public InputStream getFile(String templateFilename) throws Exception {
		String fullpathFilename = templateFilename;
		
		try {
			logger.info("Get Report Filename : {} ", templateFilename);
			logger.info("Get Report Fullpath : {} ", fullpathFilename);
		
			ClassLoader classLoader = getClass().getClassLoader();
			URL resource = classLoader.getResource(fullpathFilename);
			if (resource == null) {
				throw new IllegalArgumentException("file is not found!");
			}

			logger.info("Load template at {}", resource.getPath());

			InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(fullpathFilename);
			
			return resourceAsStream;
		} catch (Exception ex) {
			logger.info("Exception .. , fail load file at {} ", fullpathFilename);
			
			InputStream resourceAsStream = null;
			try {
				fullpathFilename = templateFilename;
				resourceAsStream = getClass().getClassLoader().getResourceAsStream(fullpathFilename);
			
				return resourceAsStream;
			} finally {
				if( resourceAsStream != null ) {
					resourceAsStream.close();
				}
			}
		}
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public Sheet getNewSheet(String sheetName) {
		// Create a Sheet
		Sheet sheet = this.workbook.createSheet(sheetName);
		return sheet;
	}
	
	public Sheet getExistingSheet(String sheetName) {
		// Create a Sheet
		Sheet sheet = this.workbook.getSheet(sheetName);
		return sheet;
	}

	
	public Cell setCellValue(Sheet sheet, int rowIndex, int colIndex, Object objValue) {
		Row row = sheet.getRow(rowIndex);
		if( row == null ) {
			row = sheet.createRow(rowIndex);
		}
		
		return this.setCellValue(row, colIndex, objValue);
	}
	
	public Cell setCellValue(Row row, int colIndex, Object objValue) {
		Cell cell = row.getCell(colIndex);
		if( cell == null ) {
			cell = row.createCell(colIndex);
		}
		
		if( objValue == null ) {
			cell.setCellValue("");
		}else if( objValue instanceof java.lang.String) {
			String tmp = (String) objValue;
			cell.setCellValue(tmp);
		}else if( objValue instanceof java.lang.Integer || objValue instanceof Long ) {
			Integer tmp = (Integer) objValue;
			cell.setCellValue(tmp);
		}else if( objValue instanceof java.util.Date){
			Date tmp = (Date) objValue;
			cell.setCellValue(tmp);
		} else if (objValue instanceof Double || objValue instanceof Float) {
			Double tmp = (Double) objValue;
			cell.setCellValue( tmp.doubleValue() );
		}else {
			logger.error(" Unknow cell data type : " + objValue.getClass() );
		}
		
		return cell;
	}

	public void overrideSheet(Sheet sheet) {
		/* CreationHelper helps us create instances of various things like DataFormat, 
        Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
		CreationHelper createHelper = workbook.getCreationHelper();
     
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		String[] columns = {"No", "Name", "Date Of Birth", "Salary"};
		
		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

		int  columnStartIndex = 0;
		// Create Other rows and cells with employees data
		int rowNum = 1; 
		int columnIndex = 1;
		List<Object[]> dataContent = new ArrayList<Object[]>();
	
	
    	Object[] obj = null;
		for (int i = 0; i < 5; i++) {
			obj = new Object[] {
				i+1,//No
				"Test" + i,
				new Date(),
				3000.00
			};
			dataContent.add(obj);
		}
		
        
		for(Object[] eachRow : dataContent ) {
			Row row = sheet.createRow(rowNum++);

			for(int col = 0; col < eachRow.length; col++ ) {
				Object objValue = eachRow[col];
				columnIndex = columnStartIndex + col;
				
				Cell cell = row.createCell(columnIndex);
				
				if( objValue == null ) {
					cell.setCellValue("");
				}else if( objValue instanceof java.lang.String) {
					String tmp = (String) objValue;
					cell.setCellValue(tmp);
				}else if( objValue instanceof java.lang.Integer || objValue instanceof Long ) {
					Integer tmp = (Integer) objValue;
					cell.setCellValue(tmp);
				}else if( objValue instanceof java.util.Date){
					Date tmp = (Date) objValue;
					cell.setCellValue(tmp);
					cell.setCellStyle(dateCellStyle);
				} else if (objValue instanceof Double || objValue instanceof Float) {
					Double tmp = (Double) objValue;
					cell.setCellValue( tmp.doubleValue() );
				}else {
					logger.error(" Unknow cell data type : " + objValue.getClass() );
				}
			}
		}
		

		// Resize all columns to fit the content size
//		for (int i = 0; i < columns.length; i++) {
//			sheet.autoSizeColumn(i);
//		}
	}
	
	public void setAutoSizeColumn(Sheet sheet) {
		int firstRowNum = sheet.getLastRowNum();
		int columnLength = sheet.getRow(firstRowNum).getLastCellNum();
		for (int i = 0; i < columnLength; i++) {
			sheet.autoSizeColumn(i);
		}
	}

	protected void setAllBorder(CellStyle style) {
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
	}

	public void writeReport(Sheet sheet, List<Object[]> contentData) throws Exception {
		this.writeReport(sheet, contentData, 0, 1);
	}
	
	public void writeReport(Sheet sheet, List<Object[]> contentData, int columnStartIndex, int rowStartIndex) {
//		logger.debug("Start writeReport : [sheetName:{}, total data {} records, startCoulumnIndex:{}, startRowIndex:{}", 
//				sheet.getSheetName(),
//				contentData == null ? 0 : contentData.size(),
//				columnStartIndex,
//				rowStartIndex
//				);
		
		/* CreationHelper helps us create instances of various things like DataFormat, 
        Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
		CreationHelper createHelper = workbook.getCreationHelper();
		
		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
	    short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
	    dateCellStyle.setDataFormat(dateFormat);
	    setAllBorder(dateCellStyle);
		
		CellStyle numberCellStyle = workbook.createCellStyle();
		short format = (short)BuiltinFormats.getBuiltinFormat("#,##0.00");
		numberCellStyle.setDataFormat(format);
		setAllBorder(numberCellStyle);
		
		CellStyle defaultCellStyle =  workbook.createCellStyle();
		setAllBorder(defaultCellStyle);
		
		this.overrideSheet(sheet);
		
		if( contentData != null ) {
			int rowIndex = 0;
			int columnIndex = 0;
			
			for(int row = 0; row < contentData.size(); row++) {
				Object[] eachRow = contentData.get(row);
				rowIndex = rowStartIndex + row;
				
				Row excelRow = sheet.getRow(rowIndex);
				if( excelRow == null ) {
					excelRow = sheet.createRow(rowIndex);
				}
				
				for(int col = 0; col < eachRow.length; col++ ) {
					Object objValue = eachRow[col];
					columnIndex = columnStartIndex + col;
					
					Cell cell = excelRow.getCell(columnIndex);
					if( cell == null ) {
						cell = excelRow.createCell(columnIndex);
					}
					
					if( objValue == null ) {
						cell.setCellValue("");
						cell.setCellStyle( defaultCellStyle );
					}else if( objValue instanceof java.lang.String) {
						String tmp = (String) objValue;
						cell.setCellValue(tmp);
						cell.setCellStyle( defaultCellStyle );
					}else if( objValue instanceof java.lang.Integer || objValue instanceof Long ) {
						Integer tmp = (Integer) objValue;
						cell.setCellValue(tmp);
						cell.setCellStyle( defaultCellStyle );
					}else if( objValue instanceof java.util.Date){
						Date tmp = (Date) objValue;
						cell.setCellValue(tmp);
						cell.setCellStyle(dateCellStyle);
					} else if (objValue instanceof Double || objValue instanceof Float) {
						Double tmp = (Double) objValue;
						cell.setCellValue( tmp.doubleValue() );
						cell.setCellStyle(numberCellStyle);
					}else {
						logger.error(" Unknow cell data type : " + objValue.getClass() );
					}
				}
			}
		}
		
		logger.debug("Finish writeReport : [sheetName:{}, total data {} records success.", sheet.getSheetName(), contentData == null ? 0 : contentData.size()
				);
	}
	
	public ByteArrayOutputStream getByteArrayOutputStream() throws IOException {
		logger.debug("Start getByteArrayOutputStream");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			workbook.write(bos);
		}catch(Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			bos.close();
			workbook.close();
			
			logger.info("Finish getByteArrayOutputStream size of the buffer : {} bytes", bos.size());
		}
		return bos;
	}
}
