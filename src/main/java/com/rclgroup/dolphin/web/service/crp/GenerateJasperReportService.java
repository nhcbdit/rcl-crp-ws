package com.rclgroup.dolphin.web.service.crp;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.rclgroup.dolphin.web.model.crp.DataPredictionReportMod;
import com.rclgroup.dolphin.web.ws.JasperReportConfig;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Service("generateJasperReportService")
public class GenerateJasperReportService {
	private Logger logger = LoggerFactory.getLogger(GenerateJasperReportService.class);
	
	private static String CRP_RPT_BY_LOC = "jasper_template/report_location.jrxml";
	private static String CRP_RPT_BY_HQ = "jasper_template/report_hq.jrxml";
	
	private static String pattern = "dd-MM-yyyy HH:mm:ss";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	
	@Autowired
	private JasperReportConfig jasperReportConfig;

	@Autowired(required = false)
	@Qualifier("dataSourceTsi") 
	private DataSource dbtDataSource;
	 
	public ByteArrayOutputStream exportByteArrayOutputStream(JasperReport jasperReport, Map params) throws JRException {

		final String METHOD_NAME = new Object() {
		}.getClass().getEnclosingMethod().getName();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			JasperPrint jasperPrint;
			jasperPrint = JasperFillManager.fillReport(jasperReport, params, dbtDataSource.getConnection());
			JasperExportManager.exportReportToPdfStream(jasperPrint, bos);
		} catch (SQLException ex) {
			logger.error(ex.getMessage(), ex);
		}

		logger.info(METHOD_NAME + ", ByteArrayOutputStream size of the buffer : {} bytes", bos.size());

		return bos;
	}

	private String toFormat(BigDecimal bd) {
		bd = bd.setScale(2, BigDecimal.ROUND_DOWN);

		DecimalFormat df = new DecimalFormat("#,###.##");
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		df.setGroupingUsed(false);
		
		return df.format(bd);
	}
	
	private String toFormat(Date dt) {
		if( dt == null ) return "";
		
		return simpleDateFormat.format(dt);
	}
	
	public ByteArrayOutputStream createCallReport(String rptType, List<DataPredictionReportMod> dataPrediction) throws Exception {
		final String METHOD_NAME = new Object() {}.getClass().getEnclosingMethod().getName();

		DataPredictionReportMod mainData = null;
		if( dataPrediction != null && dataPrediction.size() > 0) {
			mainData = dataPrediction.get(0);
		}
		
		ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
		Map<String, Object> params = new HashMap<String, Object>();
		
		
		params.put("SUBMIT_DATE", toFormat(mainData.getSubmitDate()) );
		params.put("OPR_MNG_NAME", mainData.getOprMngName());

		params.put("TERMINAL", mainData.getTerminal());
		params.put("PORT", mainData.getPort());
		params.put("VESSEL", mainData.getVessel());
		params.put("VOYAGE", mainData.getVoyage());
		params.put("OVERALL_RATING", toFormat(mainData.getOverallRating()) );
		
		params.put("SPECIAL_INSTRUCTION", mainData.getSepcialInstruction());
		params.put("DELAY_REASON", mainData.getDelayReason());

		params.put("SERVICE_PIC", mainData.getServicePic());
		params.put("SERVICE_PIC_DEP", mainData.getServicePicDep());
		params.put("SERVICE_PIC_MAIL", mainData.getServicePicMail());
		params.put("SERVICE_PIC_TEL", mainData.getServicePicTel());
		

//		params.put("REVISION_NO", "1");
//		params.put("SUBMIT_DATE", simpleDateFormat.format(new Date()) );
//		params.put("OPR_MNG_NAME", "Nut Supamala");
//
//		params.put("TERMINAL", "CNBSC");
//		params.put("PORT", "CNNGB");
//		params.put("VESSEL", "GTB");
//		params.put("VOYAGE", "491S");
//		params.put("OVERALL_RATING", toFormat(BigDecimal.TEN) );
//		
//		params.put("SPECIAL_INSTRUCTION", "English is a West Germanic language first spoken in early medieval England which eventually became a global lingua franca. It is named after the Angles");
//		params.put("DELAY_REASON", "Ready or not, English is now the global language of business. More and more multinational");
//		
//		params.put("SERVICE_PIC", "Wutiporn Kittitammarak");
//		params.put("SERVICE_PIC_DEP", "MANAGER");
//		params.put("SERVICE_PIC_MAIL", "wutiporn@rclgroup.com");
//		params.put("SERVICE_PIC_TEL", "0245745723");
		

//		DataPredictionReportMod m = null;
//		List<DataPredictionReportMod> datasourcePrediction = new ArrayList<DataPredictionReportMod>();
//		m = new DataPredictionReportMod();
//		m.setRowno(1);
//		m.setIndName("Test");
//		m.setPredictionData("2132");
//		datasourcePrediction.add(m);
//		
//		m = new DataPredictionReportMod();
//		m.setRowno(2);
//		m.setIndName("Testsadasd");
//		m.setPredictionData("99");
//		datasourcePrediction.add(m);
		
		params.put("datasourcePrediction", dataPrediction);

		try {
			JasperReport jasperReport = null;
			if( "location".equalsIgnoreCase(rptType) ) {
				params.put("REVISION_NO", "1." + mainData.getRevisionNo().toString());
				jasperReport = jasperReportConfig.getJasperReport(CRP_RPT_BY_LOC);
			} else if ( "hq".equalsIgnoreCase(rptType) ) {
				params.put("REVISION_NO", "2." + mainData.getRevisionNo().toString());
				jasperReport = jasperReportConfig.getJasperReport(CRP_RPT_BY_HQ);
			} else {
				throw new Exception("Invalid report type. [" + rptType + "]");
			}

			// JasperPrintManager.printReportToPdfStream(getJasperPrint(properties,parameters),bout);
			JasperPrint jasperPrint;
			jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
			logger.info("JasperPrint" + jasperPrint);

			// Initial Export configuration
//			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
//			configuration.setEncrypted(true);
//			configuration.set128BitKey(true);
//			configuration.setUserPassword("1234");
//			configuration.setOwnerPassword("1234");

			// JasperExportManager.exportReportToPdfStream(jasperPrint, pdfReportStream);

			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
//			pdfExporter.setConfiguration(configuration);

			pdfExporter.exportReport();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		logger.info(METHOD_NAME + ", ByteArrayOutputStream size of the buffer : {} bytes", pdfReportStream.size());

		return pdfReportStream;
	}

}
