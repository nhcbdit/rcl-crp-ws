package com.rclgroup.dolphin.web.instance;

import org.json.JSONObject;

public class VariablesInJava {

	public JSONObject STR_JSON_DATA_CONTENT;

	/**
	 * @return the sTR_JSON_DATA_CONTENT
	 */
	public JSONObject getSTR_JSON_DATA_CONTENT() {
		return STR_JSON_DATA_CONTENT;
	}
	
}
