package com.rclgroup.dolphin.web.dao.crp.proc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class PrrVoyageGet extends StoredProcedure {
	private Logger logger = LoggerFactory.getLogger(PrrVoyageGet.class);

	private static String procedureName = "PCR_VSS_CRP_VOYAGE.PRR_VOYAGE_GET";

	public PrrVoyageGet(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, procedureName);
		declareParameter(new SqlOutParameter("P_O_DATA", OracleTypes.CURSOR, new CommonResultSetExtractor()) );

		declareParameter(new SqlParameter("P_I_USER_ID", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_VOYAGE_ID", OracleTypes.NUMBER));
		declareParameter(new SqlParameter("P_I_NEXT_PREV", OracleTypes.VARCHAR));

		compile();
	}
}
