package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

public interface CrpVoyageDao {

	public Map<String, Object> findVoyageList(Map<String, Object> parameters);

	public Map<String, Object> findVoyageGetCurrentPort(Map<String, Object> parameters);

	public Map<String, Object> findVoyageGetPrediction(Map<String, Object> parameters);

	public Map<String, Object> findVoyageGetCallReportHistory(Map<String, Object> parameters);

	public Map<String, Object> findVoyageGetCallReportHistoryPrediction(Map<String, Object> parameters);

	public Map<String, Object> voyageIndUpd(Map<String, Object> parameters);

	public Map<String, Object> voyageUpd(Map<String, Object> parameters);

	public Map<String, Object> voyageSubmit(Map<String, Object> parameters);

	public Map<String, Object> monitorReport(Map<String, Object> parameters);

	public Map<String, Object> callReport(Map<String, Object> parameters);


	public class ProcedureResult {
		private String valid;
		private String errorMsg;
		public String getValid() {
			return valid;
		}
		public void setValid(String valid) {
			this.valid = valid;
		}
		public String getErrorMsg() {
			return errorMsg;
		}
		public void setErrorMsg(String errorMsg) {
			this.errorMsg = errorMsg;
		}
	}
}
