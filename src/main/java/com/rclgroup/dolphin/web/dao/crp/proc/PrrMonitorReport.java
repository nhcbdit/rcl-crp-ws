package com.rclgroup.dolphin.web.dao.crp.proc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class PrrMonitorReport extends StoredProcedure {
	private Logger logger = LoggerFactory.getLogger(PrrVoyageGet.class);

	private static String procedureName = "PCR_VSS_CRP_VOYAGE.PRR_MONITOR_REPORT";

	public PrrMonitorReport(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, procedureName);
		declareParameter(new SqlOutParameter("P_O_DATA", OracleTypes.CURSOR, new CommonResultSetExtractor()) );

		declareParameter(new SqlParameter("P_I_USER_ID", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_PORT", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_SERVICE", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_VESSEL", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_VOYAGE", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_REGION", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_COUNTRY", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_STATUS", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_SERVICE_PIC", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_ETA_FROM", OracleTypes.NUMBER));
		declareParameter(new SqlParameter("P_I_ETA_TO", OracleTypes.NUMBER));
		
		compile();
	}

}
