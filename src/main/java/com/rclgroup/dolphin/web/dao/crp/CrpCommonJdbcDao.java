package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageList;

@Repository
public class CrpCommonJdbcDao extends BaseJdbcDao implements CrpCommonDao {

	private static final String PACKAGE_NAME = "PCR_VSS_CRP_COMMON";
	private static final String PRR_VOYAGE_STATUS_LIST = "PRR_VOYAGE_STATUS_LIST";
	
	@Autowired
    public void init(@Qualifier("dataSourceTsi") DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		
        
    }
	
	public Map<String, Object> voyageStatusList(Map<String, Object> parameters) {
		
		MapSqlParameterSource inParams = super.paramToMapSqlParameter(parameters);
		
		Map<String, Object> result = executeProcedure(PACKAGE_NAME, PRR_VOYAGE_STATUS_LIST, inParams);
		return result;
	}
}
