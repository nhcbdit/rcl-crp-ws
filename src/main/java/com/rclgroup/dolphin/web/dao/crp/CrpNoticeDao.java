package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

public interface CrpNoticeDao {

	Map<String, Object> prrDequeueProcedure(Map<String, Object> parameters);

	Map<String, Object> prrQueueUpdProcedure(Map<String, Object> parameters);

}