package com.rclgroup.dolphin.web.dao.crp.proc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.util.LinkedCaseInsensitiveMap;

public class CommonResultSetExtractor implements ResultSetExtractor {
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		List<Object> list = new ArrayList<Object>();

		// collect column names
		List<String> columnNames = new ArrayList<>();
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
		    columnNames.add(rsmd.getColumnLabel(i));
		}

		int rowIndex = 0;
		while (rs.next()) {
		    rowIndex++;
		    // collect row data as objects in a List
		    List<Object> rowData = new ArrayList<>();
		    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
		        rowData.add(rs.getObject(i));
		    }
		    
		    // for test purposes, dump contents to check our results
		    // (the real code would pass the "rowData" List to some other routine)
//		    System.out.printf("Row %d%n", rowIndex);
		    
		    LinkedCaseInsensitiveMap<Object> r = new LinkedCaseInsensitiveMap<Object>();
		    
		    for (int colIndex = 0; colIndex < rsmd.getColumnCount(); colIndex++) {
		        String objType = "null";
		        String objString = "";
		        Object columnObject = rowData.get(colIndex);
		        if (columnObject != null) {
		            objString = columnObject.toString() + " ";
		            objType = columnObject.getClass().getName();
		        }
//		        System.out.printf("  %s: %s(%s)%n",
//		                columnNames.get(colIndex), objString, objType);
		        
		        if( oracle.sql.TIMESTAMP.class == (columnObject == null ? null : columnObject.getClass())) {
		        	Timestamp timestamp = rs.getTimestamp(columnNames.get(colIndex));
		        	columnObject = sdf.format(timestamp);
		        }
		        
		        r.put(columnNames.get(colIndex), columnObject);
		    }
		    
		    list.add(r);
		}
		
		return list;
	}

}
