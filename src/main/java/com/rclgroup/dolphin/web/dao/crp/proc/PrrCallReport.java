package com.rclgroup.dolphin.web.dao.crp.proc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.rclgroup.dolphin.web.model.crp.DataPredictionReportMod;

import oracle.jdbc.OracleTypes;

public class PrrCallReport  extends StoredProcedure {
	private Logger logger = LoggerFactory.getLogger(PrrCallReport.class);

	private static String procedureName = "PCR_VSS_CRP_VOYAGE.PRR_CALL_REPORT";

	public PrrCallReport(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, procedureName);
		declareParameter(new SqlOutParameter("P_O_DATA", OracleTypes.CURSOR, new RowMapperDataPredictionReport()) );

		declareParameter(new SqlParameter("P_I_USER_ID", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_VOYAGE_ID", OracleTypes.NUMBER));
		
		compile();
	}

	class RowMapperDataPredictionReport implements RowMapper<DataPredictionReportMod> {

		@Override
		public DataPredictionReportMod mapRow(ResultSet rs, int rowNum) throws SQLException {
			DataPredictionReportMod m = new DataPredictionReportMod();
			
			m.setPkVoyageId( rs.getInt("PK_VOYAGE_ID") );
			m.setPort( rs.getString("PORT") );
			m.setTerminal( rs.getString("TERMINAL") );
			m.setService( rs.getString("SERVICE") );
			m.setVessel( rs.getString("VESSEL") );
			m.setVoyage( rs.getString("VOYAGE") );
			m.setOverallRating( rs.getBigDecimal("OVERALL_RATING") );
			m.setServicePic( rs.getString("SERVICE_PIC") );
			m.setServicePicDep( rs.getString("SERVICE_PIC_DEP") );
			m.setServicePicMail( rs.getString("SERVICE_PIC_MAIL") );
			m.setServicePicTel( rs.getString("SERVICE_PIC_TEL") );
			m.setRevisionNo( rs.getInt("REVISION_NO") );
			m.setSubmitDate( rs.getDate("SUBMIT_DATE") );
			m.setOprMngName( rs.getString("OPR_MNG_NAME") );
			m.setSepcialInstruction( rs.getString("DELAY_REASON") );
			m.setDelayReason( rs.getString("DELAY_REASON") );
			m.setRowno( rs.getInt("ROWNO") );
			m.setIndName( rs.getString("IND_NAME") );
			m.setPredictionData( rs.getString("PREDICTION_DATA") );
			m.setActualData( rs.getString("ACTUAL_DATA") );
			m.setRating( rs.getBigDecimal("RATING") );

			return m;
		}
	}
}
