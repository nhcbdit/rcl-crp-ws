package com.rclgroup.dolphin.web.dao.crp.proc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rclgroup.dolphin.web.model.crp.NoticeQueue;
import com.rclgroup.dolphin.web.util.RutString;

public class PrrDequeueRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int i) throws SQLException {
		NoticeQueue mod = new NoticeQueue();
		
		mod.setQueueId( rs.getLong("QUEUE_ID"));
		mod.setVoyageId( rs.getLong("VOYAGE_ID"));
		mod.setReportType(RutString.nullToStr(rs.getString("REPORT_TYPE")));
		mod.setReportName(RutString.nullToStr(rs.getString("REPORT_NAME")));
		mod.setQueueStatus(RutString.nullToStr(rs.getString("QUEUE_STATUS")));
		mod.setQueueDate( rs.getDate("QUEUE_DATE"));
		
		return mod;
	}

}
