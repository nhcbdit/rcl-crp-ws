package com.rclgroup.dolphin.web.dao.crp.proc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class PrrVoyageIndUpd extends StoredProcedure {
	private Logger logger = LoggerFactory.getLogger(PrrVoyageIndGet.class);

	private static String procedureName = "PCR_VSS_CRP_VOYAGE.PRR_VOYAGE_IND_UPD";

	public PrrVoyageIndUpd(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, procedureName);
		declareParameter(new SqlOutParameter("P_O_VALID", OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter("P_O_ERROR_MSG", OracleTypes.VARCHAR));

		declareParameter(new SqlParameter("P_I_USER_ID", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_IND_ID", OracleTypes.NUMBER));
		declareParameter(new SqlParameter("P_I_PREDICTION_DATA", OracleTypes.VARCHAR));

		compile();
	}

}
