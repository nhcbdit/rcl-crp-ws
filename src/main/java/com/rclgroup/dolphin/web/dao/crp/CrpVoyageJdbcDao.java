package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.rclgroup.dolphin.web.dao.crp.proc.PrrCallReport;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrMonitorReport;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageGet;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageHistoryGet;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageHistoryIndGet;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageIndGet;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageIndUpd;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageList;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageSubmit;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageUpd;
import com.rclgroup.dolphin.web.util.RutLogUtil;

@Repository
public class CrpVoyageJdbcDao extends BaseJdbcDao implements CrpVoyageDao {
	
	private PrrVoyageList prrVoyageList;
	private PrrVoyageGet prrVoyageGet;
	private PrrVoyageIndGet prrVoyageIndGet;
	private PrrVoyageHistoryGet prrVoyageHistoryGet;
	private PrrVoyageHistoryIndGet prrVoyageHistoryIndGet;
	
	private PrrVoyageIndUpd prrVoyageIndUpd;
	private PrrVoyageUpd prrVoyageUpd;
	private PrrVoyageSubmit prrVoyageSubmit;
	
	private PrrMonitorReport prrMonitorReport;
	private PrrCallReport prrCallReport;
	
	
	@Autowired
    public void init(@Qualifier("dataSourceTsi") DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		
        this.prrVoyageList = new PrrVoyageList(jdbcTemplate);
        this.prrVoyageGet = new PrrVoyageGet(jdbcTemplate);
        this.prrVoyageIndGet = new PrrVoyageIndGet(jdbcTemplate);
        
        this.prrVoyageHistoryGet = new PrrVoyageHistoryGet(jdbcTemplate);
        this.prrVoyageHistoryIndGet = new PrrVoyageHistoryIndGet(jdbcTemplate);
        
        this.prrVoyageIndUpd = new PrrVoyageIndUpd(jdbcTemplate);
        this.prrVoyageUpd = new PrrVoyageUpd(jdbcTemplate);
        this.prrVoyageSubmit = new PrrVoyageSubmit(jdbcTemplate);
    	
    	
        this.prrMonitorReport = new PrrMonitorReport(jdbcTemplate);
        this.prrCallReport = new PrrCallReport(jdbcTemplate);
    }
    
	
	public Map<String, Object> findVoyageList(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageList.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> findVoyageGetCurrentPort(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageGet.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	
	public Map<String, Object> findVoyageGetPrediction(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageIndGet.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}

	
	
	public Map<String, Object> findVoyageGetCallReportHistory(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageHistoryGet.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> findVoyageGetCallReportHistoryPrediction(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageHistoryIndGet.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> voyageIndUpd(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageIndUpd.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> voyageUpd(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageUpd.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> voyageSubmit(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrVoyageSubmit.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> monitorReport(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrMonitorReport.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
	public Map<String, Object> callReport(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);
		
		Map<String, Object> result = prrCallReport.execute(parameters);
		
		RutLogUtil.dumpOutputParams(result);
		return result;
	}
	
}
