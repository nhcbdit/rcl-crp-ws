package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import com.rclgroup.dolphin.web.dao.crp.proc.PrrVoyageList;

public abstract class BaseJdbcDao {

	private static final Logger logger = LoggerFactory.getLogger(BaseJdbcDao.class);
	
	protected JdbcTemplate jdbcTemplate;
	
	protected MapSqlParameterSource paramToMapSqlParameter(Map<String, Object> parameters) {
		MapSqlParameterSource inParams = new MapSqlParameterSource();
		if (null != parameters) {
			int index = 0;
			for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
				//logger.debug("Index {} Name:{} : {}", index++, parameter.getKey(), parameter.getValue());
				inParams.addValue(parameter.getKey(), parameter.getValue());
			}
		}
		
		return inParams;
	}
	
	protected Map<String, Object> executeProcedure(String packageName, String procedureName, MapSqlParameterSource inParams) {
		
		final long start = System.currentTimeMillis();
		
		logger.info("Start Call PROCEDURE {}", procedureName);
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.jdbcTemplate.getDataSource())
				.withCatalogName(packageName)
				.withProcedureName(procedureName);

		Map<String, Object> result = simpleJdbcCall.execute(inParams);
		logger.info("PROCEDURE {} IS CALLED, Execution Time in Milliseconds: {}", procedureName, String.valueOf(System.currentTimeMillis()-start));

		return result;
	}
}
