package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.rclgroup.dolphin.web.dao.crp.proc.PrrDequeueProcedure;
import com.rclgroup.dolphin.web.dao.crp.proc.PrrQueueUpdProcedure;
import com.rclgroup.dolphin.web.util.RutLogUtil;

@Repository
public class CrpNoticeJdbcDao extends BaseJdbcDao implements CrpNoticeDao {

	private PrrDequeueProcedure prrDequeueProcedure;
	private PrrQueueUpdProcedure prrQueueUpdProcedure;
	
	@Autowired
	public void init(@Qualifier("dataSourceTsi") DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);

		this.prrDequeueProcedure = new PrrDequeueProcedure(jdbcTemplate);
		this.prrQueueUpdProcedure = new PrrQueueUpdProcedure(jdbcTemplate);
	}

	@Override
	public Map<String, Object> prrDequeueProcedure(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);

		Map<String, Object> result = prrDequeueProcedure.execute(parameters);

		//RutLogUtil.dumpOutputParams(result);
		return result;
	}

	@Override
	public Map<String, Object> prrQueueUpdProcedure(Map<String, Object> parameters) {
		RutLogUtil.dumpInputParams(parameters);

		Map<String, Object> result = prrQueueUpdProcedure.execute(parameters);

		RutLogUtil.dumpOutputParams(result);
		return result;
	}

}
