package com.rclgroup.dolphin.web.dao.crp.proc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class PrrQueueUpdProcedure extends StoredProcedure {

	private static String procedureName = "PCR_VSS_CRP_ENOTICE.PRR_QUEUE_UPD";

	public PrrQueueUpdProcedure(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, procedureName);
        declareParameter(new SqlOutParameter("P_O_VALID", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("P_O_ERROR_MSG", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("P_I_QUEUE_ID", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("P_I_FILE_NAME", OracleTypes.VARCHAR));
        compile();
    }

}
