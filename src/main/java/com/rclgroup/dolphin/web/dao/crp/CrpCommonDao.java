package com.rclgroup.dolphin.web.dao.crp;

import java.util.Map;

public interface CrpCommonDao {
	public Map<String, Object> voyageStatusList(Map<String, Object> parameters);
}
