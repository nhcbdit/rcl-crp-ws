package com.rclgroup.dolphin.web.dao.crp.proc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.rclgroup.dolphin.web.dao.crp.proc.mapper.PrrDequeueRowMapper;

import oracle.jdbc.OracleTypes;

public class PrrDequeueProcedure extends StoredProcedure {

	private static String procedureName = "PCR_VSS_CRP_ENOTICE.PRR_DEQUEUE";

	public PrrDequeueProcedure(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, procedureName);
		
		declareParameter(new SqlOutParameter("P_O_DATA", OracleTypes.CURSOR, new PrrDequeueRowMapper()));
		declareParameter(new SqlParameter("P_I_USER_ID", OracleTypes.VARCHAR));
		declareParameter(new SqlParameter("P_I_LIMIT_QUEUE", OracleTypes.INTEGER));
		
		compile();
	}
	
}