package com.rclgroup.dolphin.web.exception;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseMsgJson {

	public static String messageString(String resultCode, String resultContent, String resultMessage, String resultStatus) throws JSONException {
		
    	JSONObject json = new JSONObject();
    	json.put("resultCode", resultCode);
    	json.put("resultContent", resultContent);
    	json.put("resultMessage", resultMessage+" ERROR");
    	json.put("resultStatus", resultStatus);
    	
		return json.toString();
	}
	
	public static JSONObject messageJson(String resultCode, String resultContent, String resultMessage, String resultStatus) throws JSONException {
		
    	JSONObject json = new JSONObject();
    	json.put("resultCode", resultCode);
    	json.put("resultContent", resultContent);
    	if(resultStatus.equals("S")) {
    		json.put("resultMessage", resultMessage);
        }else {
    		json.put("resultMessage", " ERROR " + resultMessage);
        }
    	json.put("resultStatus", resultStatus);
    	
    	
		return json;
	}
	
	public static JSONObject messageJson(String strJson) throws JSONException {
		
    	JSONObject json = new JSONObject(strJson);
    	if(json.get("resultStatus").equals("F")) {
        	json.put("resultCode", "400");
    	}else {
        	json.put("resultCode", "200");
    	}

    	
		return json;
	}
}
