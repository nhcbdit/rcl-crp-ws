package com.rclgroup.dolphin.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;

import com.rclgroup.dolphin.web.ws.WebServiceBase;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

public class ServiceOutboundFilter implements ContainerResponseFilter {

	@Context
	private HttpServletRequest httpRequest;
	
	@Override
    public ContainerResponse filter(ContainerRequest creq, ContainerResponse cresp) {
		try {
			HttpSession session = httpRequest.getSession();
			synchronized(session) {
				Object counterObj = session.getAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER);
				if(counterObj != null) {
					int counter = (int)counterObj;
					if(counter > 1) {
						session.setAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER, counter-1);
					}else {
						session.removeAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT);
						session.removeAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER);
					}
				}
			}
		}catch(Exception ex) {
			//Don't abort response, just catch some potential error here
			ex.printStackTrace();
		}
		
        cresp.getHttpHeaders().putSingle("Access-Control-Allow-Origin", "*");
        cresp.getHttpHeaders().putSingle("Access-Control-Allow-Credentials", "true");
        cresp.getHttpHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD");
        cresp.getHttpHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
        
        return cresp;
    }
}
