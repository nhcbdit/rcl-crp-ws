package com.rclgroup.dolphin.web.filter;

import java.io.InputStream;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rclgroup.dolphin.web.auth.BrowserData;
import com.rclgroup.dolphin.web.exception.ResponseMsgJson;
import com.rclgroup.dolphin.web.ws.TokenAuthUtils;
import com.rclgroup.dolphin.web.ws.TokenAuthUtils.AuthResult;
import com.rclgroup.dolphin.web.ws.WebServiceBase;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

public class ServiceInboundFilter implements ContainerRequestFilter {

	@Context
	private HttpServletRequest httpRequest;

	@Override
	public ContainerRequest filter(ContainerRequest request) {
		// TODO Auto-generated method stub
		JSONObject json = null;
		try {
	        InputStream inputStream = request.getEntityInputStream();

	        StringWriter writer = new StringWriter();
	        IOUtils.copy(inputStream, writer, "UTF-8");
	        String theString = writer.toString();
	        
	        if( theString == null || theString.isEmpty() ) return request; //CRP Skip, And check each service manually.
	        
	        json = new JSONObject(theString);
	        
	        
	        BrowserData browserData;
	        browserData = new ObjectMapper().readValue(json.get("userData").toString(), BrowserData.class);

	        
//	        MultivaluedMap<String, String> headers = request.getRequestHeaders();
//	        for (Entry<String, List<String>> entry : headers.entrySet()) {
//	        	System.out.println("--- "+entry.getKey() + " : " + entry.getValue());
//	        }
	        
	        if(browserData == null) {
		    	json = ResponseMsgJson.messageJson("500", "Malformed JSON", "Data", "F");
            	throw new WebApplicationException(ResponseUtil.createJsonResponse(json.toString()));
	        }
	        
	        HttpSession session = httpRequest.getSession(true);
          //  session.setMaxInactiveInterval(12*60*60);
            AuthResult result = TokenAuthUtils.checkToken(browserData, session);
            if(result.getErrorMessage() != null) {
//            	System.err.println(result.getErrorMessage());
		    	//json = ResponseQtn.messageJson("500", "Session expire", "", "F");
            	json = ResponseMsgJson.messageJson("401", "Session expire", "", "F");
		    	
            	throw new WebApplicationException(ResponseUtil.createJsonResponse(json.toString()));
            }
            
            synchronized(session) {
            	Object counterObj = session.getAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER);
                if(counterObj == null) {
                	session.setAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER, 1);
                }else {
                	session.setAttribute(WebServiceBase.SESSION_RCL_USAGECOUNTER, (int)counterObj + 1);
                }
                session.setAttribute(WebServiceBase.SESSION_RCL_AUTHRESULT, result);
            }
            
            InputStream in = IOUtils.toInputStream(json.toString());
            request.setEntityInputStream(in);
            
            return request;
            
		} catch (WebApplicationException webEx) {
			// TODO: handle exception
			throw webEx;
		} catch (Exception ex) {
	    	try {
				json = ResponseMsgJson.messageJson("500", ex.getMessage(), "Exception", "F");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	throw new WebApplicationException(ResponseUtil.createJsonResponse(json.toString()));
        }
	}
	
	
}
