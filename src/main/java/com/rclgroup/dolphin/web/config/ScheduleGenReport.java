package com.rclgroup.dolphin.web.config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rclgroup.dolphin.web.dao.crp.CrpNoticeDao;
import com.rclgroup.dolphin.web.dao.crp.CrpVoyageDao;
import com.rclgroup.dolphin.web.model.crp.DataPredictionReportMod;
import com.rclgroup.dolphin.web.model.crp.NoticeQueue;
import com.rclgroup.dolphin.web.service.crp.GenerateJasperReportService;

@Configuration
//@ConditionalOnProperty(value = "app.config.scheduler.enabled", matchIfMissing = true, havingValue = "true")
public class ScheduleGenReport {
	private Logger logger = LoggerFactory.getLogger(ScheduleGenReport.class);

	@Autowired
	CrpNoticeDao crpNoticeJdbcDao;

	@Autowired
	CrpVoyageDao crpVoyageDao;

	@Autowired
	GenerateJasperReportService jasperReportService;

	@Value("${app.report.user:'WS'}")
	String appReportUser;

	// @Value("${app.report.filepath:D:\\temp\\report\\}")
	@Value("${app.report.filepath:/ApplTop/VAS/ATTACHMENT/}")
	String appReportFilepath;

	private static Queue<ScheduleLog> historySchedule = new LinkedList<ScheduleLog>();
	private static Queue<HistoryNoticeLog> historyNotice = new LinkedList<HistoryNoticeLog>();

	//@Scheduled(fixedRate = 3600000) // millisecond, 3,600,000 = 1 Hour
	@Scheduled(cron = "0 0 * * * ?")
	public void doSomeBackendJob() throws Exception {
		
		ScheduleLog scheduleLog = new ScheduleLog();
		scheduleLog.setActionStart(new Date());
		
		/* job implementation here */
		System.out.println("Schedule Task start : Generate Report " + new Date());

		prerequisite();
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_I_USER_ID", appReportUser);
		parameters.put("P_I_LIMIT_QUEUE", 50);

		Map<String, Object> resultOut = crpNoticeJdbcDao.prrDequeueProcedure(parameters);
		List<NoticeQueue> noticeList = (List<NoticeQueue>) resultOut.get("P_O_DATA");

		if( noticeList != null && !noticeList.isEmpty()) {
			for (NoticeQueue n : noticeList) {
				HistoryNoticeLog noticeLog = new HistoryNoticeLog();
				BeanUtils.copyProperties(n, noticeLog);
				
				try {
					System.out.println(" Notice item : queueId = " + n.getQueueId());
					String filepath = generateReport(n);

					noticeLog.setStatus(true);
					noticeLog.setResultFile(filepath);
					
					updateNoticeItem(noticeLog);

				} catch (Exception ex) {
					noticeLog.setStatus(false);
					noticeLog.setResultFile(null);
					noticeLog.setResultMessage(ex.getMessage());
					
					updateNoticeItem(noticeLog);
				}
			}
		}
		

		System.out.println("Schedule Task finish : Generate Report " + new Date());
		
		scheduleLog.setNote(" NoticeQueue " + (noticeList == null ? 0 : noticeList.size()) + " records.");
		scheduleLog.setActionFinish(new Date());
		
		addScheduleLog(scheduleLog);
		
	}

	private String generateReport(NoticeQueue noticeQueue) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("NoticeQueue : " + noticeQueue.toString()).append("\n");

		Long voyageId = noticeQueue.getVoyageId();
		String rptType = noticeQueue.getReportType();
		String reportName = noticeQueue.getReportName();
		final String FULLPATH = appReportFilepath + reportName;

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_I_USER_ID", appReportUser);
		parameters.put("P_I_VOYAGE_ID", voyageId);

		Map<String, Object> resultOut = crpVoyageDao.callReport(parameters);
		List<DataPredictionReportMod> dataPrediction = (List<DataPredictionReportMod>) resultOut.get("P_O_DATA");

		try {

			ByteArrayOutputStream baos = jasperReportService.createCallReport(rptType, dataPrediction);

			// Write file to
			sb.append("Write file to " + FULLPATH).append("\n");

			try (OutputStream outputStream = new FileOutputStream(FULLPATH)) {
				baos.writeTo(outputStream);
			}

			// Check file is exists.
			File f = new File(FULLPATH);
			if (!f.exists()) {
				throw new Exception("File does not exist, at " + FULLPATH);
			}

			sb.append("Result : Success " + FULLPATH).append("\n");
			logger.info(sb.toString());
			return FULLPATH;

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex.getMessage(), ex);

			sb.append("Result : Error " + ex.getMessage()).append("\n");

			logger.info(sb.toString());
			throw ex;
		}

	}

	
	private void updateNoticeItem(HistoryNoticeLog noticeLog) {
		addHistory(noticeLog);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_I_QUEUE_ID", noticeLog.getQueueId());
		if ( noticeLog.isStatus()) {
			//parameters.put("P_I_FILE_NAME", noticeLog.getResultFile());
			parameters.put("P_I_FILE_NAME", noticeLog.getReportName());
		} else {
			parameters.put("P_I_FILE_NAME", null);
		}

		Map<String, Object> resultOut = crpNoticeJdbcDao.prrQueueUpdProcedure(parameters);
		resultOut.put("P_O_VALID", "Y");
		resultOut.put("P_O_ERROR_MSG", "Test");

		String outValid = (String) resultOut.get("P_O_VALID");
		String outErrorMsg = (String) resultOut.get("P_O_ERROR_MSG");

		if ("Y".equals(outValid)) {
			logger.info("QueueUpdateProc : " + outValid);
		} else {
			logger.error("QueueUpdateProc : " + outValid + " errorMsg:" + outErrorMsg);
		}
	}

	
	private void prerequisite() {
		if( historySchedule.isEmpty() ) {
			ScheduleLog log = new ScheduleLog();
			log.setActionStart(new Date());
			
			try {
				// Check folder is exists.
				File f = new File(appReportFilepath);
				if (!f.exists()) {
					throw new Exception("Folder does not exist, " + appReportFilepath);
				}
				
				if( !f.isDirectory() ) {
					throw new Exception("Folder is not directory, " + appReportFilepath);
				}
				
				log.setNote("Checking, found " + appReportFilepath);
			}catch(Exception ex) {
				log.setNote("Checking, error " + ex.getMessage());
			}
			
			
			log.setActionFinish(new Date());
			
			addScheduleLog(log);
		}
	}
	
	private void addScheduleLog(ScheduleLog scheduleLog) {
		historySchedule.add(scheduleLog);
		
		if( historySchedule.size() > 20) {
			historySchedule.poll();
		}
	}
	
	private void addHistory(HistoryNoticeLog noticeLog) {
		historyNotice.add(noticeLog);
		
		if( historyNotice.size() > 20) {
			historyNotice.poll();
		}
	}
	
	
	public List<ScheduleLog> getHistorySchedule() {
		List<ScheduleLog> list = new ArrayList<ScheduleLog>(historySchedule);
		return list;
	}

	public List<HistoryNoticeLog> getHistoryNotice(){
		List<HistoryNoticeLog> list = new ArrayList<HistoryNoticeLog>(historyNotice);
		return list;
	}
	
	public static class HistoryNoticeLog extends NoticeQueue {
		private boolean status;
		
		private String resultFile;
		private String resultMessage;
		
		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}

		public String getResultFile() {
			return resultFile;
		}

		public void setResultFile(String resultFile) {
			this.resultFile = resultFile;
		}

		public String getResultMessage() {
			return resultMessage;
		}

		public void setResultMessage(String resultMessage) {
			this.resultMessage = resultMessage;
		}
	
	}
	
	public static class ScheduleLog {
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS a z")
		private Date actionStart;
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss..SSS a z")
		private Date actionFinish;
		private String note;

		
		public Date getActionStart() {
			return actionStart;
		}
		public void setActionStart(Date actionStart) {
			this.actionStart = actionStart;
		}
		public Date getActionFinish() {
			return actionFinish;
		}
		public void setActionFinish(Date actionFinish) {
			this.actionFinish = actionFinish;
		}
		public String getNote() {
			return note;
		}
		public void setNote(String note) {
			this.note = note;
		}
		
	}
}
