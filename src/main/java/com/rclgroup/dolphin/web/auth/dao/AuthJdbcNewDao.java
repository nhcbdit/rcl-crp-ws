package com.rclgroup.dolphin.web.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.rclgroup.dolphin.web.auth.LoginUser;

public class AuthJdbcNewDao extends NamedParameterJdbcDaoSupport implements IAuthDao {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public LoginUser getAdditionalUserData(String fscCode) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withProcedureName("PRR_GET_ADDITIONAL_USERDATA")
				.returningResultSet("P_O_CURSOR", new RowMapper<LoginUser>() {
					@Override
					public LoginUser mapRow(ResultSet rs, int row) throws SQLException {
						LoginUser user = new LoginUser();
						user.setFscCM(rs.getString("FSC_CM"));
						user.setControlFscCM(rs.getString("CONTROL_FSC_CM"));
						user.setLocationCM(rs.getString("LOCATION_CM"));
						user.setPointCode(rs.getString("FK_POINT_CODE"));
						user.setControlFscFlag(rs.getString("CONTROL_FSC_FLAG"));
						user.setSttcur(rs.getString("STTCUR"));
						user.setOceanPort(rs.getString("OCEAN_PORT"));
						user.setMainCurrency(rs.getString("MAIN_CURRENCY"));
						user.setIjsRoutApp(rs.getString("IJS_ROUT_APP"));
						user.setIjsDgType(rs.getString("IJS_DG_TYPE"));
						user.setIjsBackDate(rs.getString("IJS_BACK_DATE"));
						user.setIjsOldNew(rs.getString("IJS_OLD_NEW"));
						return user;
					}
				});
		
		HashMap input = new HashMap();
		input.put("P_I_FSC_CODE", fscCode);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
		
		Map<String, Object> out = jdbcCall.execute(in);
		
	    List<LoginUser> result = (List)out.get("P_O_CURSOR");
		
		if(result.size() > 0) {
			return result.get(0);
		}else {
			return null;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String mintToken(String userId, float sessionDurationMinute) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withFunctionName("FNC_BKG_MINT_TOKEN_MINUTE");
		
		HashMap input = new HashMap();
		input.put("P_I_USERID", userId);
		input.put("P_I_DURATION_MINUTE", sessionDurationMinute);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
	    
	    String result = jdbcCall.executeFunction(String.class, in);
	
	    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean verifyToken(String userId, String token, float sessionDurationMinute) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withFunctionName("FNC_VERIFY_TOKEN_MINUTE");
		
		HashMap input = new HashMap();
		input.put("P_I_USERID", userId);
		input.put("P_I_TOKEN", token);
		input.put("P_I_DURATION_MINUTE", sessionDurationMinute);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
	    
	    boolean result = jdbcCall.executeFunction(String.class, in).trim().equals("1");
	
	    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean refreshToken(String userId, String token, float sessionDurationMinute) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withFunctionName("FNC_REFRESH_TOKEN_MINUTE");
		
		HashMap input = new HashMap();
		input.put("P_I_USERID", userId);
		input.put("P_I_TOKEN", token);
		input.put("P_I_DURATION_MINUTE", sessionDurationMinute);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
	    
	    boolean result = jdbcCall.executeFunction(String.class, in).trim().equals("1");
	
	    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SessionResult getSession(String userId, String token) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withProcedureName("PRR_GET_SESSION")
				.returningResultSet("P_O_CURSOR", new JsonCursorMapper());
		
		HashMap input = new HashMap();
		input.put("P_I_USERID", userId);
		input.put("P_I_TOKEN", token);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
		
		Map<String, Object> out = jdbcCall.execute(in);
		
	    List<String> result = (List)out.get("P_O_CURSOR");
		
		return new SessionResult(out.get("P_O_SUCCESS").toString().trim().equals("1"), result.get(0));
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean saveSession(String userId, String token, String data) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withProcedureName("PRR_SAVE_SESSION");
		
		HashMap input = new HashMap();
		input.put("P_I_USERID", userId);
		input.put("P_I_TOKEN", token);
		input.put("P_I_DATA", data);
		MapSqlParameterSource in = new MapSqlParameterSource(input);
		
		Map<String, Object> out = jdbcCall.execute(in);
		
		boolean success = (out.get("P_O_RESULT").toString().trim().equals("1"));
		
		return success;
	}
	
	public void purgeExpiredSession() {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withCatalogName("PCR_BKG_AUTH")
				.withProcedureName("PRR_PURGE_EXPIRED_SESSION");
		jdbcCall.execute();
	}
	
	public String getChildFsc(String fsc) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getDataSource())
				.withFunctionName("FNC_BKG_GET_CHILD_FSC");
		HashMap<String,Object> inputMap = new HashMap<>();
		inputMap.put("P_I_FSC", fsc);
		String result = jdbcCall.executeFunction(String.class, inputMap);
		return result;
	}
	
	public static class JsonCursorMapper implements RowMapper<String> {
		@Override
		public String mapRow(ResultSet rs, int row) throws SQLException {
			return rs.getString("DATA_DETAIL");
		}
	}
}
