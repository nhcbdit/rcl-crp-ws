package com.rclgroup.dolphin.web.model.crp.reqest;

public class DataPredictionItem {

	private Integer indId;
	private String predictionData;

	public Integer getIndId() {
		return indId;
	}

	public void setIndId(Integer indId) {
		this.indId = indId;
	}

	public String getPredictionData() {
		return predictionData;
	}

	public void setPredictionData(String predictionData) {
		this.predictionData = predictionData;
	}

}
