package com.rclgroup.dolphin.web.model.crp.reqest;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.rclgroup.dolphin.web.auth.BrowserData;

public abstract class ReqBaseCommon {
	@XmlElement(name="userData")
	private BrowserData userData;

	public BrowserData getUserData() {
		return userData;
	}

	public void setUserData(BrowserData userData) {
		this.userData = userData;
	}

	
}
