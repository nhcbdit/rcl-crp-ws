package com.rclgroup.dolphin.web.model.crp;

import java.util.Date;

public class NoticeQueue {
	private Long queueId;
	private Long voyageId;

	private String reportType;
	private String reportName;
	private String queueStatus;
	private Date queueDate;

	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

	public Long getVoyageId() {
		return voyageId;
	}

	public void setVoyageId(Long voyageId) {
		this.voyageId = voyageId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getQueueStatus() {
		return queueStatus;
	}

	public void setQueueStatus(String queueStatus) {
		this.queueStatus = queueStatus;
	}

	public Date getQueueDate() {
		return queueDate;
	}

	public void setQueueDate(Date queueDate) {
		this.queueDate = queueDate;
	}

	@Override
	public String toString() {
		return "NoticeQueue [queueId=" + queueId + ", voyageId=" + voyageId + ", reportType=" + reportType
				+ ", reportName=" + reportName + ", queueStatus=" + queueStatus + ", queueDate=" + queueDate + "]";
	}
	
	

}
