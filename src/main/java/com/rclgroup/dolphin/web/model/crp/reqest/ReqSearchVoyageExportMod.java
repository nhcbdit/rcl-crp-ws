package com.rclgroup.dolphin.web.model.crp.reqest;

public class ReqSearchVoyageExportMod extends ReqSearchVoyageMod {

	private String etaFrom;
	private String etaTo;

	public String getEtaFrom() {
		return etaFrom;
	}

	public void setEtaFrom(String etaFrom) {
		this.etaFrom = etaFrom;
	}

	public String getEtaTo() {
		return etaTo;
	}

	public void setEtaTo(String etaTo) {
		this.etaTo = etaTo;
	}

}
