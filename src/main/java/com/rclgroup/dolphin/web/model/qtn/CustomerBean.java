package com.rclgroup.dolphin.web.model.qtn;

public class CustomerBean {
	private String FSC_LVL1;
	private String FSC_LVL2;
	private String FSC_LVL3;;
	private String language;;
	private String userId;
	private String mainCurr;
	private String FSC_CODE;
	private String FSC_DATE_FORMAT;
	private String country;
	private String STTCUR;
	private String DESCR;
	private String FSC_NAME;
	/**
	 * @return the fSC_LVL1
	 */
	public String getLine() {
		return FSC_LVL1;
	}
	/**
	 * @param fSC_LVL1 the fSC_LVL1 to set
	 */
	public void setLine(String fSC_LVL1) {
		FSC_LVL1 = fSC_LVL1;
	}
	/**
	 * @return the fSC_LVL2
	 */
	public String getTrade() {
		return FSC_LVL2;
	}
	/**
	 * @param fSC_LVL2 the fSC_LVL2 to set
	 */
	public void setTrade(String fSC_LVL2) {
		FSC_LVL2 = fSC_LVL2;
	}
	/**
	 * @return the fSC_LVL3
	 */
	public String getAgent() {
		return FSC_LVL3;
	}
	/**
	 * @param fSC_LVL3 the fSC_LVL3 to set
	 */
	public void setAgent(String fSC_LVL3) {
		FSC_LVL3 = fSC_LVL3;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the mainCurr
	 */
	public String getMainCurr() {
		return mainCurr;
	}
	/**
	 * @param mainCurr the mainCurr to set
	 */
	public void setMainCurr(String mainCurr) {
		this.mainCurr = mainCurr;
	}
	/**
	 * @return the fSC_CODE
	 */
	public String getFSC_CODE() {
		return FSC_CODE;
	}
	/**
	 * @param fSC_CODE the fSC_CODE to set
	 */
	public void setFSC_CODE(String fSC_CODE) {
		FSC_CODE = fSC_CODE;
	}
	/**
	 * @return the fSC_DATE_FORMAT
	 */
	public String getFSC_DATE_FORMAT() {
		if ((this.FSC_DATE_FORMAT == null) || (this.FSC_DATE_FORMAT.equals(""))) {
			return "2";
		}
		return this.FSC_DATE_FORMAT;
	}
	/**
	 * @param fSC_DATE_FORMAT the fSC_DATE_FORMAT to set
	 */
	public void setFSC_DATE_FORMAT(String fSC_DATE_FORMAT) {
		FSC_DATE_FORMAT = fSC_DATE_FORMAT;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the sTTCUR
	 */
	public String getSTTCUR() {
		return STTCUR;
	}
	/**
	 * @param sTTCUR the sTTCUR to set
	 */
	public void setSTTCUR(String sTTCUR) {
		STTCUR = sTTCUR;
	}
	/**
	 * @return the dESCR
	 */
	public String getDESCR() {
		return DESCR;
	}
	/**
	 * @param dESCR the dESCR to set
	 */
	public void setDESCR(String dESCR) {
		DESCR = dESCR;
	}
	/**
	 * @return the fSC_NAME
	 */
	public String getFSC_NAME() {
		return FSC_NAME;
	}
	/**
	 * @param fSC_NAME the fSC_NAME to set
	 */
	public void setFSC_NAME(String fSC_NAME) {
		FSC_NAME = fSC_NAME;
	}
}
