package com.rclgroup.dolphin.web.model.crp.reqest;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class ReqVoyageInfo extends ReqBaseCommon{
	private Integer voyageId;
	private String nextPrev;
	
	@JsonProperty("dataPrediction")
	private List<DataPredictionItem> dataPredictionList;

	private String specialInstruction;
	
	public Integer getVoyageId() {
		return voyageId;
	}

	public void setVoyageId(Integer voyageId) {
		this.voyageId = voyageId;
	}

	
	public String getNextPrev() {
		return nextPrev;
	}

	public void setNextPrev(String nextPrev) {
		this.nextPrev = nextPrev;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public List<DataPredictionItem> getDataPredictionList() {
		return dataPredictionList;
	}

	public void setDataPredictionList(List<DataPredictionItem> dataPredictionList) {
		this.dataPredictionList = dataPredictionList;
	}
	
	
}


