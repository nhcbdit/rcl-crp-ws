package com.rclgroup.dolphin.web.model.crp;

import java.math.BigDecimal;
import java.util.Date;

public class DataPredictionReportMod {
	private Integer pkVoyageId;
	private String port;
	private String terminal;
	private String service;
	private String vessel;
	private String voyage;
	private BigDecimal overallRating;
	private String servicePic;
	private String servicePicDep;
	private String servicePicMail;
	private String servicePicTel;
	private Integer revisionNo;
	private Date submitDate;
	private String oprMngName;
	private String sepcialInstruction;
	private String delayReason;
	private Integer rowno;
	private String indName;
	private String predictionData;
	private String actualData;
	private BigDecimal rating;

	public Integer getPkVoyageId() {
		return pkVoyageId;
	}

	public void setPkVoyageId(Integer pkVoyageId) {
		this.pkVoyageId = pkVoyageId;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getVessel() {
		return vessel;
	}

	public void setVessel(String vessel) {
		this.vessel = vessel;
	}

	public String getVoyage() {
		return voyage;
	}

	public void setVoyage(String voyage) {
		this.voyage = voyage;
	}

	public BigDecimal getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(BigDecimal overallRating) {
		this.overallRating = overallRating;
	}

	public String getServicePic() {
		return servicePic;
	}

	public void setServicePic(String servicePic) {
		this.servicePic = servicePic;
	}

	public String getServicePicDep() {
		return servicePicDep;
	}

	public void setServicePicDep(String servicePicDep) {
		this.servicePicDep = servicePicDep;
	}

	public String getServicePicMail() {
		return servicePicMail;
	}

	public void setServicePicMail(String servicePicMail) {
		this.servicePicMail = servicePicMail;
	}

	public String getServicePicTel() {
		return servicePicTel;
	}

	public void setServicePicTel(String servicePicTel) {
		this.servicePicTel = servicePicTel;
	}

	public Integer getRevisionNo() {
		return revisionNo;
	}

	public void setRevisionNo(Integer revisionNo) {
		this.revisionNo = revisionNo;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getOprMngName() {
		return oprMngName;
	}

	public void setOprMngName(String oprMngName) {
		this.oprMngName = oprMngName;
	}

	public String getSepcialInstruction() {
		return sepcialInstruction;
	}

	public void setSepcialInstruction(String sepcialInstruction) {
		this.sepcialInstruction = sepcialInstruction;
	}

	public String getDelayReason() {
		return delayReason;
	}

	public void setDelayReason(String delayReason) {
		this.delayReason = delayReason;
	}

	public Integer getRowno() {
		return rowno;
	}

	public void setRowno(Integer rowno) {
		this.rowno = rowno;
	}

	public String getIndName() {
		return indName;
	}

	public void setIndName(String indName) {
		this.indName = indName;
	}

	public String getPredictionData() {
		return predictionData;
	}

	public void setPredictionData(String predictionData) {
		this.predictionData = predictionData;
	}

	public String getActualData() {
		return actualData;
	}

	public void setActualData(String actualData) {
		this.actualData = actualData;
	}

	public BigDecimal getRating() {
		return rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

}
