package com.rclgroup.dolphin.web.model.crp;


public class FileInfo {
	private String uuid;
	private String filename;
	private String fileType;
	private byte[] byteDocument;

	public FileInfo(String uuid, String filename, String fileType, byte[] byteDocument) {
		super();
		this.uuid = uuid;
		this.filename = filename;
		this.fileType = fileType;
		this.byteDocument = byteDocument;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getByteDocument() {
		return byteDocument;
	}

	public void setByteDocument(byte[] byteDocument) {
		this.byteDocument = byteDocument;
	}

}
