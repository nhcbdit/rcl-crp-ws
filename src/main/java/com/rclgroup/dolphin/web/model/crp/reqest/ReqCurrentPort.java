package com.rclgroup.dolphin.web.model.crp.reqest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReqCurrentPort extends ReqBaseCommon{
	private Integer voyageId;
	private String nextPrev;

	public Integer getVoyageId() {
		return voyageId;
	}

	public void setVoyageId(Integer voyageId) {
		this.voyageId = voyageId;
	}

	public String getNextPrev() {
		return nextPrev;
	}

	public void setNextPrev(String nextPrev) {
		this.nextPrev = nextPrev;
	}

}
